package com.project.marketplace;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class AboutUsActivity extends AppCompatActivity {
    private Button BackButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        BackButton = (Button) findViewById(R.id.about_back);
        WebView webView = (WebView)
                findViewById(R.id.webview);


        webView.loadUrl("https://www.asos.com/about/");

        BackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AboutUsActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });
    }
}